# NavigateDeepLink 

## Summary 
This is a generic lightning component to help navigate to specific record in specific app. This code can also be used to programatically switch apps.

## Purpose
Though you can hardcode URLs to navigate to specific app or record, it may not be compatible with any future changes on URL format by salesforce. This component will help to generate redirection URL in a forward-compatible fashion.