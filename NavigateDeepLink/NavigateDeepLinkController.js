({
    init : function(cmp, event, helper) {
        var navService = cmp.find("navService");
        // Sets the route to /lightning/o/Account/home
        var pageReference = {
            type: 'standard__app',
            attributes: {
                appTarget: cmp.get("v.appName"),
                pageRef: {
                    type: "standard__recordPage",
                    attributes: {
                        recordId: cmp.get("v.recordId"),
                        objectApiName: cmp.get("v.objectName"),
                        actionName: "view"
                    }
                }
            }
        };
       console.log(pageReference);
        cmp.set("v.pageReference", pageReference);
        // Set the URL on the link or use the default if there's an error
        var defaultUrl = "#";
        navService.generateUrl(pageReference)
            .then($A.getCallback(function(url) {
                console.log(url);
                cmp.set("v.url", url ? url : defaultUrl);
            }), $A.getCallback(function(error) {
                cmp.set("v.url", defaultUrl);
            }));
    },
    handleClick: function(cmp, event, helper) {
        var navService = cmp.find("navService");
        // Uses the pageReference definition in the init handler
        var pageReference = cmp.get("v.pageReference");
        event.preventDefault();
        navService.navigate(pageReference);
    }
})